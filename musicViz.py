import re
import random
import numpy as np
import matplotlib.pyplot as plt
import collections
import os


def random_color():
    """
    :return: random rgb color tuple.
    """
    return tuple([random.randint(0, 255) for i in range(3)])


def get_lyrics(file_path):
    """
    Given a file path, returns the list of words in the lyrics file.
    :param file_path: file path to lyric file.
    :return: list of words in file.
    """
    with open(file_path, 'r') as f:
        lyrics = "".join(f.readlines())
    lyrics = lyrics.lower()
    lyrics = re.findall(r"[\w']+", lyrics)
    return lyrics


def build_array(word_list):
    """
    user to build a binary array of word matches.
    :param word_list: list of words.
    :return: binary numpy array
    """
    word_array = [[int(word1 == word2) for word2 in word_list] for word1 in word_list]
    return np.array(word_array)


def main():
    """
    Generates images of the files located in the Lyrics directory. Saves the images to the Images directory.
    :return: None
    """
    files = os.listdir("lyrics")
    for song_file in files:
        lyrics = get_lyrics(os.path.join(os.path.dirname(__file__), "lyrics", song_file))
        unique_lyrics = collections.Counter(lyrics).keys()
        word_colors = {}
        for word in unique_lyrics:
            word_colors[word] = random_color()

        music_array = build_array(lyrics)

        color_array = []
        for row in music_array:
            color_row = []
            for i,col in enumerate(row):
                if col:
                    color_row.append(word_colors[lyrics[i]])
                else:
                    color_row.append((0, 0, 0))
            color_array.append(color_row)

        color_array = np.array(color_array, dtype='float64')
        plt.imshow(color_array)
        plt.axis('off')
        fp = os.path.join(os.path.dirname(__file__), "images", "%s.png"%(song_file))
        plt.savefig(fp, bbox_inches='tight')
    return


if __name__=="__main__":
    main()